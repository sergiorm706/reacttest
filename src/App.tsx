import {ProtectedRoute} from "./components/ProtectedRoute"
import Dashboard from "./pages/Dashboard"
import Admin from "./pages/Admin"

import Home from "./pages/Home"
import Analytic from "./pages/Analytic"
import Landing from "./pages/Landing"
import Login from "./components/Login"
import Navigation from "./components/navbar/Navbar"
import NotFoundPage from "./pages/NotFoundPage"
import UsersPage from "./pages/UsersPage"

import { BrowserRouter,Routes,Route, Navigate } from "react-router-dom";
import { useState } from "react";
import UserPage from "./pages/UserPage"

interface User {
  id: number;
  name: string;
  permissions: string[];
  roles: string[];
}

function App() {
  const [user, setUser] = useState<User | null>(null);
  // const [user,setUser] = useState(null)

  const login = () => {
    setUser({
      id: 1,
      name: "Sergio",
      permissions: ['analize1'],
      roles: ['admin1']
    })
  }

  const logout = () => setUser(null)

  return (
    <BrowserRouter>
    <Navigation/>
    {
      user ? (
        <button onClick={logout}>Logout</button>
      ):(
        <button onClick={login}>Login</button>
      )
    }
    <Routes>
      <Route path="/" element={<Landing/>}></Route>
      <Route path="/landing" element={<Landing/>}></Route>
      <Route path="/login" element={<Login/>}></Route>


      <Route path="/home" element={
      <ProtectedRoute isAllowed={!!user}>
        <Home/>
      </ProtectedRoute>
      } />

      <Route element={<ProtectedRoute isAllowed={!!user}/>}>
          <Route path="/dashboard/*" element={<Dashboard/>} />
          <Route path="/dashboard/*" element={<Dashboard/>}>
                <Route path="welcome" element={<p>Welcome</p>}></Route>
          </Route>
        
          {/* <Route path="/home" element={<Home/>} /> */}


      </Route>
          <Route path="/usuarios" element={<Navigate to="/users"/>}></Route>
          {/* <Route path="/usuarios" element={<Navigate replace to="/users"/>}></Route> */}
          <Route path="/users" element={<UsersPage/>}></Route>
          <Route path="/users/:id" element={<UserPage/>}></Route>

      <Route path="/analytic" element={
      <ProtectedRoute isAllowed={!!user && user.permissions.includes('analize')} redirectTo="/home">
        <Analytic/>
      </ProtectedRoute>
      } />

      <Route path="/admin" element={
      <ProtectedRoute isAllowed={!!user && user.roles.includes('admin')} redirectTo="/home">
        <Admin/>
      </ProtectedRoute>
      } />


       <Route path="*" element={<NotFoundPage/>}></Route>


      </Routes>

    </BrowserRouter>
    
  )
}

export default App