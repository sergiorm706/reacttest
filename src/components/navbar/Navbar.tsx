import { Link,NavLink,useLocation } from "react-router-dom";
import "./navbar.css";

export default function Navigation() {
  const { pathname } = useLocation();

    return <nav>
      <ul>
        <li>
          <NavLink className={pathname === '/landing' ? 'active' : ''} to={'/landing'}>Landing</NavLink>
        </li>
        <li>
          <Link className={pathname === '/home' ? 'active' : ''} to={'/home'}>home</Link>
        </li>
        <li>
          <Link className={pathname === '/analytic' ? 'active' : ''} to={'/analytic'}>analytic</Link>
        </li>
        <li>
          <Link className={pathname === '/dashboard' ? 'active' : ''} to={'/dashboard'}>Dashboard</Link>
        </li>
        <li>
          <Link className={pathname === '/admin' ? 'active' : ''} to={'/admin'}>Admin</Link>
        </li>
        <li>
          <Link className={pathname === '/login' ? 'active' : ''} to={'/login'}>login</Link>
        </li>
        <li>
          <NavLink className={pathname === '/users' ? 'active' : ''} to={'/users'}>users</NavLink>
        </li>
      </ul>
    </nav>
  }