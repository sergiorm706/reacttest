import { Navigate,Outlet } from "react-router-dom";

export const ProtectedRoute = ({ isAllowed,children,redirectTo="/landing" }: any) =>{
    if(!isAllowed){
        return <Navigate to={redirectTo} />
    }
    return children ?? <Outlet/>
}