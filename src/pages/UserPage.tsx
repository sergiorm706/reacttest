import { useParams } from "react-router-dom";

export default function UserPage() {
  // const params = useParams()
  const {id} = useParams()
  // console.log(params)
    return (
      // <h1>User Page {params.id} (private)</h1>
      <h1>User Page {id} (private)</h1>
    )
}