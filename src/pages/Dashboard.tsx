// import Header  from "../components/Header"
// import Aside  from "../components/Aside"
// import Content from "../components/Content"
// import Footer from "../components/Footer"

import { Link, useNavigate,Outlet } from "react-router-dom"



export default function Dashboard() {

    const navigate = useNavigate()

    const handlerClick = () =>{
        navigate("/")
    }

    return (
       <div>
        <h1>Dashboard</h1>
        <Link to="welcome"> link welcome </Link>
        <Outlet/>
        {/* <Routes>
          <Route path="welcome" element={<p>Welcome</p>}></Route>
        </Routes> */}
        <button onClick={handlerClick}>
            Logout
        </button>
       {/* <Header />
      <Aside />
      <Content />
      <Footer />  */}
     </div>
    )
}