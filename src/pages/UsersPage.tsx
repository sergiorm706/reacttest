import { Link } from "react-router-dom";

const userId = 10
export default function UsersPage() {
    return (
      <div>

      <h1>Users Page (private)</h1>
      <Link to={`/users/${userId}`}>User Page {userId} (private)</Link>
      </div>
    )
}